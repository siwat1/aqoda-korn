const { Service } = require("./Service");

class HotelApplication {
  constructor() {
    this.service = new Service();
  }

  createHotel(floorCount, roomPerFloorCount) {
    const hotel = this.service.initializeHotel(floorCount, roomPerFloorCount);

    console.log(
      `Hotel created with ${hotel.floorCount} floor(s), ${
        hotel.roomPerFloorCount
      } room(s) per floor.`
    );
  }

  book(name, age, roomId) {
    try {
      const keyCardId = this.service.bookByRoomId(name, age, roomId);
      const bookingDetail = `Room ${roomId} is booked by ${name} with keycard number ${keyCardId}.`;

      console.log(bookingDetail);
    } catch (error) {
      console.log(error.message);
    }
  }

  bookByFloor(name, floorNumber) {
    try {
      const [roomIds, keyCardIds] = this.service.bookByFloorNumber(
        name,
        floorNumber
      );
      const bookingDetail = `Room ${roomIds.join(
        ", "
      )} are booked with keycard number ${keyCardIds.join(", ")}`;

      console.log(bookingDetail);
    } catch (error) {
      console.log(error.message);
    }
  }

  checkOut(name, keyCardId) {
    try {
      const roomId = this.service.checkOutByKeyCardId(name, keyCardId);
      const checkOutDetail = `Room ${roomId} is checkout.`;

      console.log(checkOutDetail);
    } catch (error) {
      console.log(error.message);
    }
  }

  checkOutGuestByFloor(floorNumber) {
    try {
      const roomIds = this.service.checkOutByFloorNumber(floorNumber);
      const checkOutDetail = `Room ${roomIds.join(", ")} are checkout.`;

      console.log(checkOutDetail);
    } catch (error) {
      console.log(error.message);
    }
  }

  listGuest() {
    const guestNames = this.service.guestNames;

    console.log(guestNames.join(", "));
  }

  listAvailableRooms() {
    const availableRoomIds = this.service.availableRoomIds;

    console.log(availableRoomIds.join(", "));
  }

  getGuestInRoom(roomId) {
    try {
      const guestName = this.service.getGuestNameByRoomId(roomId);

      console.log(guestName);
    } catch (error) {
      console.log(error.message);
    }
  }

  listGuestByFloor(floorNumber) {
    const guestNames = this.service.getGuestNamesByFloorNumber(floorNumber);

    console.log(guestNames.join(", "));
  }

  getGuestByAge(operator, age) {
    const guestNames = this.service.getGuestNamesByAge(operator, age);

    console.log(guestNames.join(", "));
  }
}

exports.HotelApplication = HotelApplication;
