const { Hotel } = require("./models/Hotel");
const { Customer } = require("./models/Customer");

class Service {
  initializeHotel(floorCount, roomPerFloorCount) {
    this.hotel = new Hotel(floorCount, roomPerFloorCount);

    return this.hotel;
  }

  bookByRoomId(name, age, roomId) {
    const customer = new Customer(name, age);
    const [_, keyCardIssueHistory] = this.hotel.bookAndCheckIn(
      customer,
      roomId
    );
    const keyCardId = keyCardIssueHistory.keyCardId;

    return keyCardId;
  }

  bookByFloorNumber(customerName, floorNumber) {
    const customer = new Customer(customerName);
    const [
      bookingHistories,
      keyCardIssueHistories
    ] = this.hotel.bookAndCheckInByFloorNumber(customer, floorNumber);
    const roomIds = bookingHistories.map(
      bookingHistory => bookingHistory.roomId
    );
    const keyCardIds = keyCardIssueHistories.map(
      keyCardIssueHistory => keyCardIssueHistory.keyCardId
    );

    return [roomIds, keyCardIds];
  }

  checkOutByKeyCardId(name, keyCardId) {
    const customer = this.createCheckOutCustomer(name, keyCardId);
    const roomId = customer.latestReceivedKeyCard.roomId;

    this.hotel.checkOutByCustomer(customer);

    return roomId;
  }

  checkOutByFloorNumber(floorNumber) {
    const roomIds = this.hotel
      .getBookedRoomsByFloorNumber(floorNumber)
      .map(room => room.id);

    this.hotel.checkOutByFloorNumber(floorNumber);

    return roomIds;
  }

  createCheckOutCustomer(name, keyCardId) {
    const keyCard = this.hotel.getKeyCardByKeyCardId(keyCardId);
    const roomId = keyCard.roomId;
    const bookingHistory = this.hotel.getBookingHistoryByRoomId(roomId);
    const customerName = bookingHistory.customerInfo.name;
    const customerAge = bookingHistory.customerInfo.age;
    const nameMatchWithCustomerName = customerName === name;

    if (!nameMatchWithCustomerName) {
      throw new Error(
        `Only ${customerName} can checkout with keycard number ${keyCardId}`
      );
    }

    const customer = new Customer(customerName, customerAge);

    customer.receiveKeyCard(keyCard);

    return customer;
  }

  getGuestNamesByAge(operation, age) {
    const filterCondition = this.getAgeFilterConditionByOperation(
      operation,
      age
    );

    return this.getGuestNamesByCondition(filterCondition);
  }

  getAgeFilterConditionByOperation(operation, age) {
    switch (operation) {
      case "<":
        return bookingHistory => {
          const customerAge = bookingHistory.customerInfo.age;

          return customerAge && customerAge < age;
        };
      case "=":
        return bookingHistory => {
          const customerAge = bookingHistory.customerInfo.age;

          return customerAge && customerAge === age;
        };
      case ">":
        return bookingHistory => {
          const customerAge = bookingHistory.customerInfo.age;

          return customerAge && customerAge > age;
        };
    }
  }

  getGuestNamesByCondition(filterFunction) {
    return Array.from(
      new Set(
        this.hotel.notCheckedOutBookingHistories
          .filter(filterFunction)
          .map(bookingHistory => bookingHistory.customerInfo.name)
      )
    );
  }

  getGuestNameByRoomId(id) {
    const guestName = this.hotel.notCheckedOutBookingHistories.find(
      bookingHistory => bookingHistory.roomId === id
    ).customerInfo.name;

    if (!guestName) {
      throw new Error("Guest Not Found");
    }

    return guestName;
  }

  getGuestNamesByFloorNumber(floorNumber) {
    function filterCondition(bookingHistory) {
      return bookingHistory.floorNumberOfRoom === floorNumber;
    }

    return this.getGuestNamesByCondition(filterCondition);
  }

  get availableRoomIds() {
    return this.hotel.availableRooms.map(room => room.id);
  }

  get guestNames() {
    const guestNames = this.hotel.notCheckedOutBookingHistories.map(
      bookingHistory => bookingHistory.customerInfo.name
    );

    return guestNames;
  }
}

exports.Service = Service;
