const fs = require("fs");
const { Command } = require("./models/Command");
const { HotelApplication } = require("./HotelApplication");

function main() {
  const hotelApplication = new HotelApplication();
  const fileName = "input.txt";
  const commands = getCommandsFromFileName(fileName);

  commands.forEach(command => {
    switch (command.name) {
      case "create_hotel":
        const [floorCount, roomPerFloorCount] = command.params;

        hotelApplication.createHotel(+floorCount, +roomPerFloorCount);
        break;
      case "book": {
        const [roomId, name, age] = command.params;

        hotelApplication.book(name, +age, roomId);
        break;
      }
      case "book_by_floor": {
        const [floorNumber, name] = command.params;

        hotelApplication.bookByFloor(name, floorNumber);
        break;
      }
      case "checkout": {
        const [keyCardId, name] = command.params;

        hotelApplication.checkOut(name, keyCardId);
        break;
      }
      case "checkout_guest_by_floor": {
        const [floorNumber] = command.params;

        hotelApplication.checkOutGuestByFloor(floorNumber);
        break;
      }
      case "list_available_rooms": {
        hotelApplication.listAvailableRooms();
        break;
      }
      case "list_guest": {
        hotelApplication.listGuest();
        break;
      }
      case "get_guest_in_room": {
        const [roomId] = command.params;

        hotelApplication.getGuestInRoom(roomId);
        break;
      }
      case "list_guest_by_age": {
        const [operator, age] = command.params;

        hotelApplication.getGuestByAge(operator, +age);
        break;
      }
      case "list_guest_by_floor": {
        const [floorNumber] = command.params;

        hotelApplication.listGuestByFloor(floorNumber);
        break;
      }
      default:
        break;
    }
  });
}

function getCommandsFromFileName(fileName) {
  const file = fs.readFileSync(fileName, "utf-8");

  return file
    .split("\n")
    .map(line => line.split(" "))
    .map(([commandName, ...params]) => new Command(commandName, params));
}

main();
