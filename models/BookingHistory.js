class BookingHistory {
  constructor(customerInfo, roomId) {
    this.customerInfo = customerInfo;
    this.roomId = roomId;
    this.isCheckedOut = false;
  }

  checkOut() {
    this.isCheckedOut = true;
  }

  get floorNumberOfRoom() {
    return this.roomId.substr(0, 1);
  }
}

exports.BookingHistory = BookingHistory;
