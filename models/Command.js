class Command {
  constructor(command, params) {
    this.name = command;
    this.params = params;
  }
}

exports.Command = Command;
