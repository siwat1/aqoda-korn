class Customer {
  constructor(name, age = undefined, keyCards = []) {
    this.name = name;
    this.age = age;
    this._keyCards = keyCards;
  }

  get keyCards() {
    if (!this._keyCards) {
      throw new Error("KeyCard Not Found");
    }

    return this._keyCards;
  }

  get latestReceivedKeyCard() {
    return this.keyCards[this.keyCards.length - 1];
  }

  receiveKeyCard(keyCard) {
    this._keyCards.push(keyCard);
  }

  returnKeyCards() {
    const keyCards = this.keyCards;
    this._keyCards = [];

    return keyCards;
  }
}

exports.Customer = Customer;
