class CustomerInfo {
  constructor(name, age=undefined) {
    this.name = name;
    this.age = age;
  }
}

exports.CustomerInfo = CustomerInfo;
