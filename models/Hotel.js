const { Room } = require("./Room");
const { KeyCard } = require("./KeyCard");
const { CustomerInfo } = require("./CustomerInfo");
const { BookingHistory } = require("./BookingHistory");
const { KeyCardIssueHistory } = require("./KeyCardIssueHistory");

class Hotel {
  constructor(floorCount, roomPerFloorCount) {
    this.floorCount = floorCount;
    this.roomPerFloorCount = roomPerFloorCount;
    this.totalRoomCount = floorCount * roomPerFloorCount;
    this.rooms = this.createRooms(floorCount, roomPerFloorCount);
    this._keyCards = this.createKeyCards(this.totalRoomCount);
    this.bookingHistories = [];
    this.keyCardIssueHistories = [];
  }

  createRooms(floorCount, roomPerFloorCount) {
    function flatten(accumulate, elem) {
      return accumulate.concat(elem);
    }

    return Array.from(Array(floorCount))
      .map((_, firstIndex) =>
        Array.from(Array(roomPerFloorCount)).map((_, secondIndex) => {
          const floorNumber = firstIndex + 1;
          const roomNumber = secondIndex + 1;
          const id = `${floorNumber}0${roomNumber}`;

          return new Room(id);
        })
      )
      .reduce(flatten, []);
  }

  createKeyCards(amount) {
    return Array.from(Array(amount)).map((_, index) => {
      const id = `${index + 1}`;

      return new KeyCard(id);
    });
  }

  bookAndCheckIn(customer, roomId) {
    const bookingHistory = this.book(customer, roomId);
    const keyCardIssueHistory = this.checkIn(customer, bookingHistory);

    return [bookingHistory, keyCardIssueHistory];
  }

  bookAndCheckInByFloorNumber(customer, floorNumber) {
    if (!this.isFloorAvailable(floorNumber)) {
      throw new Error(`Cannot book floor ${floorNumber} for ${customer.name}.`);
    }

    const roomIds = this.getRoomsByFloorNumber(floorNumber).map(
      room => room.id
    );
    const [bookingHistories, keyCardIssueHistories] = roomIds.map(roomId =>
      this.bookAndCheckIn(customer, roomId)).reduce((acc, histories) => {
        acc[0].push(histories[0]);
        acc[1].push(histories[1]);
        
        return acc;
      },[[],[]]);

    return [bookingHistories, keyCardIssueHistories];
  }

  book(customer, roomId) {
    if (this.canBook(customer, roomId)) {
      const customerInfo = new CustomerInfo(customer.name, customer.age);
      const room = this.getRoomById(roomId);
      const bookingHistory = new BookingHistory(customerInfo, room.id);

      room.book();

      this.bookingHistories.push(bookingHistory);

      return bookingHistory;
    }
  }

  checkIn(customer, bookingHistory) {
    const [keyCard, keyCardIssueHistory] = this.issueKeyCard(bookingHistory);

    customer.receiveKeyCard(keyCard);

    return keyCardIssueHistory;
  }

  canBook(customer, roomId) {
    if (this.isFull) {
      throw new Error("Hotel is fully booked.");
    }

    const room = this.getRoomById(roomId);

    if (room.isBooked) {
      const customerName = customer.name;
      const currentCustomerName = this.getBookingHistoryByRoomId(room.id)
        .customerInfo.name;

      throw new Error(
        `Cannot book room ${
          room.id
        } for ${customerName}, The room is currently booked by ${currentCustomerName}.`
      );
    }

    return true;
  }

  issueKeyCard(bookingHistory) {
    const keyCard = this.firstAvailableCard;
    const roomId = bookingHistory.roomId;

    keyCard.writeData(roomId);

    const keyCardIssueHistory = new KeyCardIssueHistory(
      bookingHistory.customerInfo,
      keyCard.id,
      keyCard.roomId
    );

    this.keyCardIssueHistories.push(keyCardIssueHistory);

    return [keyCard, keyCardIssueHistory];
  }

  checkOutByCustomer(customer) {
    const keyCard = customer.latestReceivedKeyCard;
    const room = this.getRoomById(keyCard.roomId);

    this.clearRoom(room);
    this.clearKeyCard(keyCard);
  }

  checkOutByFloorNumber(floorNumber) {
    const rooms = this.getBookedRoomsByFloorNumber(floorNumber);
    const keyCards = this.getUsedKeyCardsByFloorNumber(floorNumber);

    rooms.forEach(room => this.clearRoom(room));
    keyCards.forEach(keyCard => this.clearKeyCard(keyCard));
  }

  clearRoom(room) {
    room.makeAvailable();
    this.getBookingHistoryByRoomId(room.id).checkOut();
  }

  clearKeyCard(keyCard) {
    keyCard.deleteData();
    this.getKeyCardIssueHistoryByKeyCardId(keyCard.id).returnKeyCard();
  }

  getRoomById(id) {
    const room = this.rooms.find(room => room.id === id);

    if (!room) {
      throw new Error("Room Not Found");
    }

    return room;
  }

  getBookedRoomsByFloorNumber(floorNumber) {
    const rooms = this.rooms.filter(
      room => room.isBooked && room.floorNumber === floorNumber
    );

    return rooms;
  }

  getRoomsByFloorNumber(floorNumber) {
    const rooms = this.rooms.filter(room => room.floorNumber === floorNumber);

    return rooms;
  }

  getBookingHistoryByRoomId(id) {
    const bookingHistory = this.notCheckedOutBookingHistories.find(
      bookingHistory => bookingHistory.roomId === id
    );

    if (!bookingHistory) {
      throw new Error("Key Card Not Found");
    }

    return bookingHistory;
  }

  getBookingHistoriesByFloorNumber(floorNumber) {
    return this.notCheckedOutBookingHistories.filter(
      bookingHistory => bookingHistory.floorNumberOfRoom === floorNumber
    );
  }

  getKeyCardIssueHistoryByKeyCardId(id) {
    const keyCardIssueHistory = this.notReturnedKeyCardIssueHistories.find(
      keyCardIssueHistory => keyCardIssueHistory.keyCardId === id
    );

    if (!keyCardIssueHistory) {
      throw new Error("KeyCard History Not Found");
    }

    return keyCardIssueHistory;
  }

  getKeyCardIssueHistoriesByFloorNumber(floorNumber) {
    return this.notReturnedKeyCardIssueHistories.filter(
      keyCardIssueHistory =>
        keyCardIssueHistory.floorNumberOfRoom === floorNumber
    );
  }

  getKeyCardByKeyCardId(id) {
    const keyCard = this.keyCards.find(keyCard => keyCard.id === id);

    if (!keyCard) {
      throw new Error("Key Card Not Found");
    }

    return keyCard;
  }

  getUsedKeyCardsByFloorNumber(floorNumber) {
    const keyCards = this.keyCards.filter(
      keyCard => keyCard.isUsed && keyCard.floorNumberOfRoom === floorNumber
    );

    if (keyCards.length === 0) {
      throw new Error("Key Card Not Found");
    }

    return keyCards;
  }

  isFloorAvailable(floorNumber) {
    return this.getBookedRoomsByFloorNumber(floorNumber).length === 0;
  }

  get notCheckedOutBookingHistories() {
    return this.bookingHistories.filter(
      bookingHistory => !bookingHistory.isCheckedOut
    );
  }

  get notReturnedKeyCardIssueHistories() {
    return this.keyCardIssueHistories.filter(
      keyCardIssueHistory => !keyCardIssueHistory.isReturnedKeyCard
    );
  }

  get keyCards() {
    function ascendingOrder(a, b) {
      return +a.id - +b.id;
    }

    return this._keyCards.sort(ascendingOrder);
  }

  get firstAvailableCard() {
    return this.keyCards.find(keyCard => !keyCard.isUsed);
  }

  get availableRooms() {
    return this.rooms.filter(eachRoom => !eachRoom.isBooked);
  }

  get isFull() {
    return this.availableRooms.length === 0;
  }
}

exports.Hotel = Hotel;
