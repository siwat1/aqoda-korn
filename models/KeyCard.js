class KeyCard {
  constructor(id) {
    this.id = id;
    this.isUsed = false;
  }

  get roomId() {
    if (!this._roomId) {
      throw new Error("KeyCard Data Not Found");
    }

    return this._roomId;
  }

  get floorNumberOfRoom() {
    return this.roomId.substr(0, 1);
  }

  writeData(roomId) {
    this._roomId = roomId;
    this.isUsed = true;
  }

  deleteData() {
    this._roomId = undefined;
    this.isUsed = false;
  }
}

exports.KeyCard = KeyCard;
