class KeyCardIssueHistory {
  constructor(customerInfo, keyCardId, roomId) {
    this.customerInfo = customerInfo;
    this.keyCardId = keyCardId;
    this.roomId = roomId;
    this.isReturnedKeyCard = false;
  }

  returnKeyCard() {
    this.isReturnedKeyCard = true;
  }
}

exports.KeyCardIssueHistory = KeyCardIssueHistory;
