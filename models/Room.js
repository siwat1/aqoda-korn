class Room {
  constructor(id) {
    this.id = id;
    this.isBooked = false;
  }

  get floorNumber() {
    return this.id.substr(0,1);
  }

  book() {
    this.isBooked = true;
  }

  makeAvailable() {
    this.isBooked = false;
  }
}

exports.Room = Room;
